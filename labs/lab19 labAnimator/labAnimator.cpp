//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "labAnimator.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button4Click(TObject *Sender)
{
	laGroup->Position->X = - laGroup->Width;
	laFIO->Position->X = this->Width + laFIO->Width;
	TAnimator::AnimateIntWait(laGroup, "Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(laFIO, "Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);

}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button5Click(TObject *Sender)
{
	laName->AutoSize = true;
	laName->TextSettings->Font->Size = 400;
	TAnimator::AnimateIntWait(laName, "TextSettings.Font.Size", 96, 1,TAnimationType::Out, TInterpolationType::Linear);
    laName->AutoSize = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button6Click(TObject *Sender)
{
	int y = GridPanelLayout1->Position->Y;
	GridPanelLayout1->Position->Y = -GridPanelLayout1->Height;
	TAnimator::AnimateIntWait(GridPanelLayout1, "Position.Y",y,1,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button7Click(TObject *Sender)
{
    laEralash->RotationAngle = 0;
	TAnimator::AnimateIntWait(laEralash, "RotationAngle",720,2,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------


