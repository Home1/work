//---------------------------------------------------------------------------

#ifndef game1H
#define game1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <System.Math.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buReset;
	TButton *buAbout;
	TLayout *ly;
	TRectangle *Rectangle1;
	TLabel *laCorrect;
	TRectangle *Rectangle2;
	TLabel *laWrong;
	TLabel *laAnswer;
	TRectangle *reRemember;
	TLabel *laNumber;
	TProgressBar *pbRemember;
	TLabel *laRemember;
	TEdit *edAnswer;
	TButton *buAnswer;
	TTimer *tiRemember;
	TButton *buZoomOut;
	TButton *buZoomIn;
	void __fastcall tiRememberTimer(TObject *Sender);
	void __fastcall buResetClick(TObject *Sender);
	void __fastcall buAnswerClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall edAnswerKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall buZoomOutClick(TObject *Sender);
	void __fastcall buZoomInClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private: 	// User declarations
	int FCountCorrect;
	int FCountWrong;
	int FSecretNumber;
	void DoReset();
	void DoContinue();
	void DoAnswer();
	void DoViewQuestion(bool aValue);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif

