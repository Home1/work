//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "game1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
	 FCountCorrect = 0;
	 FCountWrong = 0;
	 DoContinue();
}
//---------------------------------------------------------------------------
void Tfm::DoContinue()
{
	laCorrect->Text = "����� " + IntToStr(FCountCorrect);
	laWrong->Text = "������� " + IntToStr(FCountWrong);
	FSecretNumber = RandomRange(100000, 999999);
	laNumber->Text = IntToStr(FSecretNumber);
	edAnswer->Text = "";
	pbRemember->Value = 0;
	DoViewQuestion(true);
}
//---------------------------------------------------------------------------
void Tfm::DoAnswer()
{
	int x = StrToIntDef(edAnswer->Text, 0);
	if ( x == FSecretNumber ){
		FCountCorrect++;
	}
	else {
		FCountWrong++;
	}
	DoContinue();
}
//---------------------------------------------------------------------------
void Tfm::DoViewQuestion(bool aValue)
{
	tiRemember->Enabled = aValue;
	laRemember->Visible = aValue;
	reRemember->Visible = aValue;
	pbRemember->Visible = aValue;
	laAnswer->Visible =! aValue;
	edAnswer->Visible =! aValue;
	buAnswer->Visible =! aValue;
	if (edAnswer->Visible) {
		edAnswer->SetFocus();
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tiRememberTimer(TObject *Sender)
{
  pbRemember->Value = pbRemember->Value + 1;
  if ( pbRemember->Value >= pbRemember->Max ){
    DoViewQuestion(false);
  }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buResetClick(TObject *Sender)
{
 DoReset();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAnswerClick(TObject *Sender)
{
    DoAnswer();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	Randomize();
	DoReset();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::edAnswerKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if ( Key == vkReturn){
		DoAnswer();
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buZoomOutClick(TObject *Sender)
{
	ly->Scale->X -= 0.1;
	ly->Scale->Y -= 0.1;
	ly->RecalcSize();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buZoomInClick(TObject *Sender)
{
	ly->Scale->X += 0.1;
	ly->Scale->Y += 0.1;
	ly->RecalcSize();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage("Dugarov");
}
//---------------------------------------------------------------------------
