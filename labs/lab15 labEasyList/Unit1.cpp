//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop
#include <System.IOUtils.hpp>
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;

//
const UnicodeString clist[] = {"one","two", "three"};

//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void Tfm::LoadItem(int aIndex)
{
	UnicodeString xName = System::Ioutils::TPath::GetDocumentsPath() + PathDelim + clist[aIndex];
	im->Bitmap->LoadFromFile(xName + ".jpg");
	TStringList *x = new TStringList;
	__try{
		x->LoadFromFile(xName + ".txt");
		Text1->Text = x->Text;
	}
	__finally {
		x->DisposeOf();
	}
}
void __fastcall Tfm::ListBox1ItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	ScrollBox1->ViewportPosition = TPointF(0,0);
	LoadItem(Item->Index);
	Text1->RecalcSize();
	tc->ActiveTab = tiIcon;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
    tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------
