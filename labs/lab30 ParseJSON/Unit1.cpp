//---------------------------------------------------------------------------

#include <fmx.h>
#include <System.JSON.hpp>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	TJSONObject *xObj;
	TJSONArray *xArr;

	xObj = (TJSONObject*)TJSONObject::ParseJSONValue(meJSON->Text);

	xObj = (TJSONObject*)TJSONObject::ParseJSONValue(xObj->GetValue("response")->ToString());
	Memo2->Lines->Add("count: " + xObj->GetValue("count")->Value());

	xArr = (TJSONArray*)TJSONObject::ParseJSONValue(xObj->GetValue("items")->ToString());

	xObj = (TJSONObject*)TJSONObject::ParseJSONValue(xArr->Items[0]->ToString());
	Memo2->Lines->Add("id: " + xObj->GetValue("id")->Value());
	Memo2->Lines->Add("title: " + xObj->GetValue("title")->Value());

	xObj->DisposeOf();
	xArr->DisposeOf();
}
//---------------------------------------------------------------------------

