//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	 FListBox = new TList;
	 for (int i = 1; i <= cMaxBox; i++) {
		 FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));
	 }
	 FListAnswer = new TList;
	 FListAnswer->Add(Button1);
	 FListAnswer->Add(Button2);
	 FListAnswer->Add(Button3);
	 FListAnswer->Add(Button4);
	 FListAnswer->Add(Button5);
	 FListAnswer->Add(Button6);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormDestroy(TObject *Sender)
{
	delete FListBox;
	delete FListAnswer;
}
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val + (float)30/(24*60*60);
	tmPlay->Enabled = true;
	DoContinue();
}
void Tfm::DoContinue()
{
	for (int i = 0; i < cMaxBox; i++) {
		TRectangle *xRectangle;
		xRectangle = (TRectangle*)FListBox->Items[i];
		xRectangle->Fill->Color = TAlphaColorRec::Lightgray;
	}
	FNumberCorrect = RandomRange(cMinPossible,cMaxPossible);
	int *xRandom = RandomArrayUnique(cMaxBox, FNumberCorrect);
	TRectangle *xRectangle;
	for (int i = 0; i < FNumberCorrect; i++) {
		xRectangle = (TRectangle*)FListBox->Items[xRandom[i]];
		xRectangle->Fill->Color = TAlphaColorRec::Lightblue;
	}
	int xAnswerStart = FNumberCorrect - Random(cMaxAnswer-1);
	if (xAnswerStart < cMinPossible) {
		xAnswerStart = cMinPossible;
	}
	TButton *xButton;
	for (int i = 0; i < cMaxAnswer; i++) {
		xButton = (TButton*)FListAnswer->Items[i];
		xButton->Text = IntToStr(xAnswerStart + i);
	}
}
void Tfm::DoAnswer(int aValue)
{
	(aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
     DoContinue();
}
void Tfm::DoFinish()
{
	tmPlay->Enabled = false;
}

void __fastcall Tfm::buAnswerAllClick(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	if (x <= 0) {
		DoFinish();
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button7Click(TObject *Sender)
{
	DoReset();
}
//---------------------------------------------------------------------------

