//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val + (float)30/(24*60*60);
	Timer1->Enabled = true;
	DoContinue();
}

void Tfm::DoContinue()
{
	const UnicodeString c1 = "02468";
	const UnicodeString c2 = "13579";
	const UnicodeString c3 = L"aeiou";
	const UnicodeString c4 = L"bcdfghijklmnpqrstvwxyz";
	laCorrect->Text = Format(L"����: %d", ARRAYOFCONST((FCountCorrect)));
	bool xEven = (Random(2) == 1);
	bool xVowel = (Random(2) == 1);
	bool xBox360 =  (Random(2) == 1);
	UnicodeString xResult = "";
	if (xEven) {
		xResult += c1.SubString0(Random(c1.Length()), 1);
	} else {
	   xResult += c2.SubString0(Random(c2.Length()), 1);
	}
	if (xVowel) {
		xResult += c3.SubString0(Random(c3.Length()), 1);
	} else {
	   xResult += c4.SubString0(Random(c4.Length()), 1);
	}
	if (xBox360) {
		FAnswerCorrect = xEven;
		laBox1->Text = xResult;
		laBox2->Text = "";
	} else {
		FAnswerCorrect = xVowel;
		laBox1->Text = "";
		laBox2->Text = xResult;
	}
}

void Tfm::DoAnswer(bool AValue)
{
	(AValue == FAnswerCorrect) ? FCountCorrect++ : FCountWrong++;
	DoContinue();
}
void Tfm::DoFinish()
{
	Timer1->Enabled = false;
	laFinishCorrect->Text = Format(L"���������: %d", ARRAYOFCONST((FCountCorrect)));
	laFinishWrong->Text = Format(L"������������: %d", ARRAYOFCONST((FCountWrong)));
	tc->ActiveTab = TFinish;
}

void __fastcall Tfm::Timer1Timer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text = FormatDateTime("nn:ss", x);
	if (x <= 0) {
		 DoFinish();
	}
}

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = TMenu;
}

void __fastcall Tfm::buPlayClick(TObject *Sender)
{
	tc->ActiveTab = TPlay;
    DoReset();
}

void __fastcall Tfm::buHelpClick(TObject *Sender)
{
    tc->ActiveTab = THelp;
}

void __fastcall Tfm::buExitClick(TObject *Sender)
{
    Close();
}

void __fastcall Tfm::buBackbuBackClick(TObject *Sender)
{
    tc->ActiveTab = TMenu;
}

void __fastcall Tfm::buYesClick(TObject *Sender)
{
	DoAnswer(true);
}

void __fastcall Tfm::buNoClick(TObject *Sender)
{
    DoAnswer(false);
}

void __fastcall Tfm::Button2Click(TObject *Sender)
{
	tc->ActiveTab = TPlay;
    DoReset();
}

void __fastcall Tfm::Button3Click(TObject *Sender)
{
	tc->ActiveTab = TMenu;
}

void __fastcall Tfm::buBackClick(TObject *Sender)
{
    tc->ActiveTab = TMenu;
}

