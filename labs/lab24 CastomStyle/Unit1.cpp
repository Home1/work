//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::AcClearExecute(TObject *Sender)
{
	im->Bitmap->SetSize(0,0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TakePhotoFromLibraryAction1DidFinishTaking(TBitmap *Image)

{
     im->Bitmap->Assign(Image);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TakePhotoFromCameraAction1DidFinishTaking(TBitmap *Image)

{
	im->Bitmap->Assign(Image);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ShowShareSheetAction1BeforeExecute(TObject *Sender)
{
	  ShowShareSheetAction1->Bitmap->Assign(im->Bitmap);
}
//---------------------------------------------------------------------------
