//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "paswd.h"
#include "pswd.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPasswordClick(TObject *Sender)
{
	edPassword->Text = RandomStr(StrToIntDef(edLength->Text,9),
		skLower->IsChecked, skUpper->IsChecked, skNumber->IsChecked,
		 skSpec->IsChecked);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
    ShowMessage("Created by A.Dolgikh");
}
//---------------------------------------------------------------------------

