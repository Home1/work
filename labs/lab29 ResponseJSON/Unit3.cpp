//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buJSON1Click(TObject *Sender)
{
	RESTRequest1->Execute();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buJSON2Click(TObject *Sender)
{
	RESTRequest2->Execute();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buJSON3Click(TObject *Sender)
{
	RESTRequest3->Execute();
}
//---------------------------------------------------------------------------




