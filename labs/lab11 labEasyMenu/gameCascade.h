//---------------------------------------------------------------------------

#ifndef gameCascadeH
#define gameCascadeH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiPlay;
	TTabItem *tiHelp;
	TLayout *Layout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TLabel *Label1;
	TButton *Button5;
	TToolBar *ToolBar2;
	TLabel *Label2;
	TButton *Button6;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TButton *Button11;
	TButton *Button12;
	TToolBar *tb1;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall ButtonBackClick(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm1 *fm1;
//---------------------------------------------------------------------------
#endif
