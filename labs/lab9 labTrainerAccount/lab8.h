//---------------------------------------------------------------------------

#ifndef lab8H
#define lab8H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tb;
	TButton *buReboot;
	TButton *buInfo;
	TLayout *ly;
	TGridPanelLayout *gplUp;
	TRectangle *reGood;
	TLabel *laGood;
	TRectangle *reBad;
	TLabel *laBad;
	TLabel *laPrimer;
	TLabel *laVerno;
	TGridPanelLayout *gplDown;
	TButton *buDa;
	TButton *buNet;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buRebootClick(TObject *Sender);
	void __fastcall buDaClick(TObject *Sender);
	void __fastcall buNetClick(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
private:
	bool FAnswerCorrect;
	int FCountCorrect;
	int FCountWrong;
	void DoReset();
	void DoContinue();
    void DoAnswer(bool aValue);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
