//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TPie *Pie1;
	TRectangle *Rectangle1;
	TEllipse *Ellipse1;
	void __fastcall Ellipse1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall Ellipse1MouseEnter(TObject *Sender);
	void __fastcall Ellipse1MouseLeave(TObject *Sender);
	void __fastcall Ellipse1MouseMove(TObject *Sender, TShiftState Shift, float X, float Y);
	void __fastcall Ellipse1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);

private:
	float FX, FY;
	bool FIsDragging;	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
