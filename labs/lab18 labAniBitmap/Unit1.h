//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Objects.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TImage *Image1;
	TImage *Image2;
	TBitmapAnimation *bma1;
	TBitmapAnimation *bma2;
private:	// User declarations
public:		// User declarations
	__fastcall Tfm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm1 *fm1;
//---------------------------------------------------------------------------
#endif
