//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit3.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormShow(TObject *Sender)
{
	dm->FDConnection->Connected = true;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAddClick(TObject *Sender)
{
	dm->taNotes->Append();
    tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------


void __fastcall Tfm::lvItemClick(TObject *Sender)
{
	dm->taNotes->Edit();
	tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buSaveClick(TObject *Sender)
{
	dm->taNotes->Post();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buCanselClick(TObject *Sender)
{
	dm->taNotes->Cancel();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDeleteClick(TObject *Sender)
{
	dm->taNotes->Delete();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

