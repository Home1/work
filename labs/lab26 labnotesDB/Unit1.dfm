object dm: Tdm
  OldCreateOrder = False
  Height = 295
  Width = 400
  object FDConnection: TFDConnection
    Params.Strings = (
      'Database=C:\Users\VD\Desktop\06.12 BD\Notes.db'
      'DriverID=sQLite')
    LoginPrompt = False
    AfterConnect = FDConnectionAfterConnect
    BeforeConnect = FDConnectionBeforeConnect
    Left = 64
    Top = 32
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 216
    Top = 56
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 216
    Top = 176
  end
  object taNotes: TFDTable
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'Notes'
    TableName = 'Notes'
    Left = 80
    Top = 160
    object taNotesCAPTION: TStringField
      FieldName = 'CAPTION'
      Origin = 'CAPTION'
      Required = True
      Size = 50
    end
    object taNotesPRIORITY: TSmallintField
      FieldName = 'PRIORITY'
      Origin = 'PRIORITY'
      Required = True
    end
    object taNotesDETAIL: TStringField
      FieldName = 'DETAIL'
      Origin = 'DETAIL'
      Size = 500
    end
  end
end
